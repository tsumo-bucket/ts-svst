@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminProfile') }}">Profile</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>Edit</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
    <div class="row">
      <div class="col-sm-12">
        <div class="caboodle-card">
          <div class="caboodle-card-header">
            <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
          </div>
          <div class="caboodle-card-body">
            {!! Form::model($user, ['route'=>['adminProfileUpdate'], 'method' => 'patch', 'class'=>'form form-parsley']) !!}
            @include('admin.profile.form')
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection